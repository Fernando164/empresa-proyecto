<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->timestamps();

            
        });


        DB::table('roles')->insert([
                'id' => 1,
                'nombre' => 'Gerente estrategico',
            ]);

            DB::table('roles')->insert([
                'id' => 2,
                'nombre' => 'Auxilar administrativo',

            ]);

            DB::table('roles')->insert([
                'id' => 3,
                'nombre' => 'Profecional de proyectos',
                
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rols');
    }
}
