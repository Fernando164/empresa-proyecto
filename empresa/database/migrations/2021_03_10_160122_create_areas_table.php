<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->timestamps();
        });

        DB::table('areas')->insert([
                'id' => 1,
                'nombre' => 'administracion',
            ]);

            DB::table('areas')->insert([
                'id' => 2,
                'nombre' => 'Contabilidad',

            ]);

            DB::table('areas')->insert([
                'id' => 3,
                'nombre' => 'Sistemas',
                
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas');
    }
}
