
<!DOCTYPE html>
<html>
<style type="text/css">
	body {
  box-sizing: border-box;
}
</style>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<title>
		@yield('title') 
	</title>
</head>

<body>
	
	@yield('content')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
	<script type="text/javascript">
		$(function(){
			var $create = $("create"); 
			if ($create.length) {
				$create.validate({
					rules:{
						fname:{
							required: true
						}
					},
					messages:{
						fname:{
							required: 'hello'
						}
					}
 				})
			}
		})
	</script>
</body>
</html>
