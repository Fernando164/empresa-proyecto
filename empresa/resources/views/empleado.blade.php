@extends('layouts/layouts')

@section('title', 'Facultades') <!-- titulo de la de la pestaña -->

@section('content')


<div class="container">	
	<br>
	<h1>Crear empleado</h1>
	<form method="post" action="{{route('empleado.store')}}">
		{{csrf_field()}}
		<div class="form-group row" >
			<label for="inputEmail3" class="col-sm-2 col-form-label" style="text-align: right;" >Nombre completo*</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="inputEmail3" name="nombre" placeholder="Nombre completo del empleado" required>
			</div>
		</div>
		<div class="form-group row" >
			<label for="inputPassword3" class="col-sm-2 col-form-label" style="text-align: right;" >Correo electronico*</label>
			<div class="col-sm-10">
				<input type="email" class="form-control" id="inputPassword3" name="email" placeholder="Correo electronico" required>
			</div>
		</div>
		<fieldset class="form-group">
			<div class="row">
				<legend class="col-form-label col-sm-2 pt-0" style="text-align: right;">Sexo*</legend>
				<div class="col-sm-10">
					<div class="form-check">
						<input class="form-check-input" type="radio" name="sexo" value="M" id="gridRadios1"  checked>
						<label class="form-check-label" for="gridRadios1">
							Masculino
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="sexo" value="F" id="gridRadios2" >
						<label class="form-check-label" for="gridRadios2">
							Femenino
						</label>
					</div>
				</div>
			</div>
		</fieldset>
		<div class="form-group row">
			<div class="col-sm-2" style="text-align: right;">Area</div>
			<div class="col-sm-10">
				<select class="custom-select" id="" name="area">
					@foreach($areas as $area)
					<option value="{{$area->id}}">{{$area->nombre}}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-2" style="text-align: right;">Descripcion</div>
			<div class="col-sm-10">
				<textarea class="form-control" id="description" name="descripcion" required></textarea> 
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-2" style="text-align: right;">boletin</div>
			<div class="col-sm-10">
				<div class="form-check">
					<input class="form-check-input" type="checkbox" id="boletin" name="boletin" value="1">
					<label class="form-check-label" for="gridCheck1">
						boletin
					</label>
				</div>
			</div>
		</div>
		<fieldset class="form-group">
			<div class="row">
				<legend class="col-form-label col-sm-2 pt-0" style="text-align: right;">Roles*</legend>
				<div class="col-sm-10">
					@foreach($roles as $rol)
					<div class="form-check">
						<input class="form-check-input" type="radio" name="rol" value="{{$rol->id}}" id="gridRadios1" v checked>
						<label class="form-check-label" for="gridRadios1">
							{{$rol->nombre}}
						</label>
					</div>
					@endforeach	
					
				</div>
			</div>
		</fieldset>
		
		<div class="form-group row">
			<div class="col-sm-2"></div>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-primary">Crear</button>
			</div>
		</div>

	</form>

	<table class="table">
		<thead class="thead-dark">
			<tr>
				<th scope="col">Nombre</th>
				<th scope="col">Email</th>
				<th scope="col">Sexo</th>
				<th scope="col">Boletín</th>
			</tr>
		</thead>
		<tbody>
			
			@foreach($empleados as $empleado)
			<tr>
				<td>{{$empleado->nombre}}</td>
				<td>{{$empleado->email}}</td>

				@if($empleado->sexo == 'M')	
				<td>Masculino</td>
				@elseif($empleado->sexo == 'F')
				<td>Femenino</td>
				@endif		
				
				@if($empleado->boletin == 1)	
				<td>Sí</td>
				@else
				<td>No</td>
				@endif		 
			</tr>
			@endforeach

			


		</tbody>
	</table>


</div>


