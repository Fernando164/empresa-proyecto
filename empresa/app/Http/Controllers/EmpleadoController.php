<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Empleado;
use App\Rol;
use App\Area;
use App\Empleado_rol;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $areas = Area::all();
        $roles = Rol::all();
        $empleados = Empleado::all();
        return view('empleado', compact('roles', 'areas', 'empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Empleado = new Empleado;
        $Empleado->nombre = $request->nombre;
        $Empleado->email = $request->email;
        $Empleado->sexo = $request->sexo;
        $Empleado->boletin = $request->boletin;
        $Empleado->area_id = $request->area;
        $Empleado->descripcion = $request->descripcion;
        

        if ($Empleado->save()) {

            /* toma la id del registro guardado*/
            $id_empleado = $Empleado->id; 

            $Empleado_rol = new Empleado_rol;
            $Empleado_rol->empleado_id = $id_empleado;
            $Empleado_rol->rol_id = $request->rol;

            if ($Empleado_rol->save()) {
                return redirect(route('empleado.index'));
            }
            


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
